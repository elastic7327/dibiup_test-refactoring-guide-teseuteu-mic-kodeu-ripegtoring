![Screen Shot 2017-05-10 at 10.06.15 AM.png](https://bitbucket.org/repo/4ppq4Xr/images/4094448824-Screen%20Shot%202017-05-10%20at%2010.06.15%20AM.png)

![Screen Shot 2017-05-10 at 9.57.07 AM.png](https://bitbucket.org/repo/4ppq4Xr/images/2679543747-Screen%20Shot%202017-05-10%20at%209.57.07%20AM.png)

## 디비업 테스트 및 코드 리팩토링 가이드(TDD & Refactoring guide)
### v.1.0.1
### 지속적으로 수정 업데이트 중입니다.


### TDD(Test Driven Development)의 철학
 테스트(test) -----> 성공(green) -----> 리팩토링 

```
#!python
# views.py
# 함수형 뷰를 예를 들겠습니다 . . .
# 가령 func 라는 함수를 . . . .

def func(request):
    return HttpResponse("Hello World")

```

### 이런식으로
```
#!python
#views.py
# 이런식으로 리턴값만 바꿔주어도, 리팩토링이라 말해줄 수 있습니다.
def func(request):
    return HttpResponse("I'M So AWESOME!")
```
#
자 우리는 방금 func 라는 장고 뷰 함수를 리팩토링 했습니다.
하지만 이런식의 리팩토링은 굉장히 무모하고, 사고로 이어질수 있습니다.
만약에 방금처럼 뷰 함수가 무척 간단하다면, 테스트를 하지않고 리팩토링을 하는 유혹에 쉽게 빠지게 됩니다.



# 2. 모델 테스트

# 2.1 뷰 테스트


# 3. 코드 커버리지(code coverage)